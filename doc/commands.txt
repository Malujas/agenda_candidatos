
//Instalar DDBB
sudo apt install mysql-server mysql-client
sudo apt install php-mysql

//Acces com a superusuari
sudo mysql

//Crear base de dades
create database threads;

//Crear usuari
create user 'threadsuser' identified by '1234Threads!';

//Donar permisos al usuari
grant all on threads.* to 'threadsuser'@'%';

//Sortir de mysql com a super usuari
exit


//Acces com a usuari threadsuser
mysql -u threadsuser -p

//Dins del mysql, seleccionem base de dades
use threads

//Creacio taula
create table thread ( 
    idt INT PRIMARY KEY AUTO_INCREMENT, 
    threads INT, durability VARCHAR(255), 
    color VARCHAR(255));

//Insercio dades dummy
insert into empleat values ('Carlos Martinez', 'CTO', 20/12/1989, '10.699,00€'),
('Eduardo Gonzalez', 'Encarregat Tècnic', 19/05/1999,  '550,00 €'),
('Sonia Hernandez', 'Directora d'Informàtica', 31/08/1975, '1.400,00€'),
('Roberto Garcia', 'Vicepresident', 06/05/2000,  '4.600,00€'),
('Yolanda Muñoz', 'Directora Executiva', 04/04/1996, '6.500,00€');

//Consulta
select * from thread;
select * from thread\G

//Afegir columna
alter table thread add column img_filename varchar(255);

