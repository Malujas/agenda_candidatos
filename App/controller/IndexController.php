<?php

require_once(__DIR__.'/../model/Contactos.php');
require_once(__DIR__.'/../model/db/ContactosDb.php');

class IndexController{

    public function listContactos(){
        $db = new ContactosDb();
        $conts = $db->listContactos();
        return $conts;
    }

    public function detailsContactos($pos = 0){
       $db = new ContactosDb();
       return $db->getContacto($pos);
    }

    public function createContacto($nm, $aplls, $tel, $corr, $act){
        $db = new ContactosDb();
        $contacto = $db->insertContactos($nm, $aplls, $tel, $corr, $act);
        return $db -> getContacto($nm, $aplls, $tel, $corr, $act);
     }

     public function updateContactos($nm, $aplls, $tel, $corr, $act){
        $db = new ContactosDb();
        return $db->updateRegContactos($nm, $aplls, $tel, $corr, $act);
        $cont = $db->getContacto($i);
     }

     public function deleteContactos($i){
        $db = new ContactosDb();
        $cont = $db->getContacto($i);

        $db->removeContacto($i);

        return $cont;
     }

}
