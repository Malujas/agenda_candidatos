<?php

class Contactos{

    private $nombre;
    private $apellidos;
    private $telefono;
    private $correo;
    private $activo;
    private $id;
    

    public function __construct($nm = "Carlos", $aplls = "Munoz Garcia", $tel = 625987415, $corr = "cmunoz@gmail.com" , $act='Y', $i = null){
        $this->setNombre($nm);
        $this->setApellidos($aplls);
        $this->setTelefono($tel);
        $this->setCorreo($corr);
        $this->setActivo($act);
        $this->setId($i);
        
    }

    public function getId(){
        return $this->id;
    }

    public function getNombre(){
        return $this->$nombre;
    }

    public function getApellidos(){
        return $this->$apellidos;
    }

    public function getTelefono(){
        return $this->$telefono;
    }

    public function getCorreo(){
        return $this->$correo;
    }

    public function getActivo(){
        return $this->$activo;
    }

    
   
    public function setId($v){
        $this->$id = $v;
    }

    public function setNombre($v){
        $this->$nombre = $v;
    }

    public function setApellidos($v){
        return $this->$apellidos = $v;
    }

    public function setTelefono($v){
        $this->$telefono = $v;
    }

    public function setCorreo($v){
        $this->$correo = $v;
    }  

    public function setActivo($v){
        $this->$activo = $v;
    }
}
