<?php

require_once(__DIR__.'/../Contactos.php');


class ContactosDb{

    private $_conn;

    private $nm ="";
    private $aplls;
    private $tel;
    private $corr;
    private $act;

    
    
    public function listContactos(){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
         
        //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
        $query = "SELECT * FROM contactos";
        $stmt = $this->_conn->prepare($query);
        
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
        
        
        //RETRIEVE RESULTS AND BUILD RETURN ARRAY
        $conts = array();
        while ($cont = $res->fetch_assoc()  !==null) {
          array_push($conts, new contactos($cont['Nombre'], $cont['Apellidos'],
              $cont['Telefono'], $cont['Correo'], $cont['Activo'], $cont['id']));
        }
        return $conts;
      }

      public function getContacto($id){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "SELECT * FROM contactos WHERE id = ?";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PARAMETER
        $stmt->bind_param($nm, $aplls, $tel, $corr, $act);
        if (!empty($nm)) {
          echo $nm = $nombre;
        } 

        if (!empty($aplls)) {
          echo $aplls = $apellidos;
        } 
        
        if (!empty($tel)) {
          echo $tel = $telefono;
        } 
        
        if (!empty($corr)) {
          echo $corr = $correo;
        }
        
        if (!empty($act)) {
          echo $act = $activo;
        }
        
    
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        $contcs = $res->fetch_assoc();
        return new contactos($contcs['Nombre'], $contcs['Apellidos'], 
            $contcs['Telefono'], $contcs['Correo'], $contcs['Activo'], $contcs['id']);
      }

      public function insertContactos($nombre, $apellidos, $telefono, $correo, $activo){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "INSERT INTO contactos (Nombre, Apellidos, Telefono, Correo, Activo) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this ->_conn->prepare($query);

        //DEFINE PARAMETER
        $stmt->bind_param("ssis", $nm, $aplls, $tel, $corr, $act);
       if (!empty($nm)) {
         echo $nm = $nombre;
       } 
        $aplls = $apellidos;
        $tel = $telefono;
        $corr = $correo;
        $act = $activo;
        
       if(!empty($aplls)){
            echo $aplls;
            }

        if(!empty($tel)){
              echo $tel;
              }

        if(!empty($corr)){
            echo $corr;
            }

        if(!empty($act)){
            echo $act;
            }
    
        //EXECUTE QUERY
        $stmt->execute();
        if($stmt === false){
          return false;
        }else{
          return true;
        }

        //echo('ID insert');
        //var_dump($stmt->insert_id);
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getContacto();
      }

      
      public function updateRegContactos($nombre, $apellidos, $telefono, $correo, $activo, $id){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ALL THE AVAILABLE PARAMETERS
        $query = "UPDATE contactos SET nombre = ?, apellido = ?, telefono = ?, correo = ?, activo = ?,  WHERE idt = ? ";
        $stmt = $this ->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param($nm, $aplls, $tel, $corr, $act, $i);
        $nm = $nombre;
        $aplls = $apellidos;
        $tel = $telefono;
        $corr = $correo;
        $act = $activo;
        $i = $id;
    
        //EXECUTE QUERY
        $stmt->execute();
        if($stmt === false){
          return false;
        }else{
          return true;
        }

        if(!isset($nm)){
          $nm = 'Variable Nombre is not set';
          }
          
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getContacto($id);
      }

      public function removeContacto($id){
          //OPEN CONNECTION TO DATABASE
          $this->openConnection();
  
          //PREPARE STATEMENT WITH ONE PARAMETER
          $query = "DELETE FROM contactos WHERE idt = ?";
          $stmt = $this ->_conn->prepare($query);
  
          //DEFINE PAFAMETER
          $srtmt->bind_param("i", $index);
          $index = $id;
      
          //EXECUTE QUERY
          $stmt->execute();
          return $stmt=true;
      }


    private function openConnection(){
        if($this->_conn == NULL){
          $this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DB); 
        }
    }

}
