<?php

require_once(__DIR__.'/../App/inc/constants.php');
require_once(__DIR__.'/../App/controller/IndexController.php');

$cnt = new IndexController();
?>

<html>
  <head>
    <title>Añadir Contacto</title>
  </head>
  <body>
    <h1>Añadir Contacto</h1>
    <form id="thform" method="post" action="/forms/add.php" enctype="multipart/form-data">
    <dl>
        <dt><b><label for="addt-nombre">Nombre:</label></b></dt>
        <dd><input type="text" id="addt-nombre" name="nombre" tabindex="1"/></dd>

        <dt><b><label for="addt-apellidos">Apellidos:</label></b></dt>
        <dd><input type="text" id="addt-apellidos" name="apellidos" tabindex="2"/></dd>

        <dt><b><label for="addt-telefono">Telefono:</label></b></dt>
        <dd><input type="text" id="addt-telefono" name="telefono" tabindex="3"/></dd>

        <dt><b><label for="addt-correo">Correo Electronico:</label></b></dt>
        <dd><input type="text" id="addt-correo" name="correo" tabindex="4" valeu="$" /></dd>

        <dt><b><label for="addt-activo">Activo:</label></b></dt>
        <dd><input type="radio" id="addt-activo" name="activo" tabindex="5" value="Y"/>Si
        <input type="radio" id="addt-activo" name="activo" tabindex="5" value="N"/>No</dd>
        <br>
    </dl>
    </form>
    <button><a href="forms/add.php">Añadir Contacto</a></button><br>
    <br>
    <button><a href="index.php">Volver al inicio</a></button>
  </body>
</html>