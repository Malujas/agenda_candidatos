<?php

require_once(__DIR__.'/../../App/inc/constants.php');
require_once(__DIR__.'/../../App/controller/IndexController.php');

//RECUPEREM DADES
$id = $_GET['index'];

$cnt = new IndexController();
$es = $cnt->removeContacto($id);

?><html>
  <head>
    <title>Eliminar Contacto</title>
    <script>
    function confirmar_accion() {
      var txt;
     var pregunta= confirm("Seguro que quieres eliminar este contacto?");
     if(pregunta==true){
        txt=alert("El contacto se ha eliminado correctamente");
     }else{
       txt=alert("Operación cancelada");
     }
    }
      
    </script>
  </head>
  <body>
    <h1>Se ha eliminado correctamente</h1>
    <li> <?=$es->getNombre()?>;</li>
    <ul>
      <li><?=$es->getApellidos()?></li>
      <li><?=$es->getTelefono()?></li>
      <li><?=$es->getCorreo()?></li>
      <li><?=$es->getActivo()?></li>
    </ul>
    <button><a href="index.php">Volver al inicio</a></button>
  </body>
</html>